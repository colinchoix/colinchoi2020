# 2020-05-08

3 hours

* Read Abram's Email
* Review some stuff

***

# 2020-05-11

6 hours
 
* Meeting to discuss project
* Read Solving Substitution Ciphers with Combined Language Models
* Read Decoding Anagrammed Texts Written in an Unknown Language and Script
* Read On the Naturalness of Software
* Read NLP Smoothing tutorial
* Read Dorabella Wikipedia
* Set up Linux VM and cloned repos

# 2020-05-12

4 hours

* Read NLP Language Modeling with N-Grams
* Learning about music theory
* Examined a few of the files in the dorabella bitbucket repo

# 2020-05-13

4 hours

* Reviewed Solving Substitution Ciphers and Decoding Anagrammed Texts papers
* Looked through perl code related to those 2 papers
* I was able to get the scripts to solve some simple substitution ciphers
* Continued learning about music theory

# 2020-05-14

8 hours

* Encountered some odd issues with my virtual box VM. Spent a few hours trying to fix it. 
* Ended up partitioning my drive and installing Ubuntu onto my computer
* Spent the rest of the day going through the dorabella repo
* Read through the code and ran the ones I could. Had troubles getting a few to run due to python version

# 2020-05-15

6 hours

* Changed a few of the python files in the dorabella repo to python3
* Had a meeting with Professor Kondrak discussing things I can do
* Went through HHK Decipherment repo and got the perl scripts to run on my computer
* Took the dorabella cipher and attempted to translate it to english using the perl scripts

# 2020-05-16

3 hours

* Got 2 english decipherments of the dorabella cipher using the perl scripts
* Script took about 3 hours to run and decipherment had some english words but didnt make much sense
* Started learning some perl so it would be easier to understand how the scripts work

***

# 2020-05-19

4 hours

* Read The Use of Project Gutenberg and Hexagram Statistics to Help Solve Famous Unsolved Ciphers
* Learnt some more perl and explored the perl script options
* Ran beam search perl script and tried deciphering with a different C value in the mcts perl script

# 2020-05-20

5 hours

* Weekly Dorabella meeting discussing what to work on
* Read Decipherment with a Million Random Restarts
* Looked through Anirudh dorabella files 
* Looking into how kenlm works to create language models
 
# 2020-05-21

6 hours

* WSD Meeting
* Installed kenlm lmplz
* Figured out how to use lmplz to create arpa files
* Ran lmplz on dorabella cipher to get a language model

# 2020-05-22

6 hours

* Copied udhr samples and code to my local machine
* Looked through the perl scripts that came with the udhr corpus
* Researched more about what the data in arpa files mean and how to use kenlm
* Began writing a program to create random texts using the arpa file obtained from the dorabella cipher

# 2020-05-23

2 hours

* More research on using kenlm
* Wrote a python program that can generate random text given an arpa file

# 2020-05-24

2 hours

* Attemped to decode the random texts that I generated
* Ran the hk-langid perl script on dorabella to find potential languange it might be in

***

# 2020-05-25

5 hours

* Dorabella meeting and standup meetings
* Used the language model option to attempt language identification of dorabella
* Experimented creating random texts with an arpa file
* Currently I'm creating a random text by testing each letter and building the text based on the perplexity value of the sentence it creates
* Figuring out how I can use kenlm to create a probability distribution to choose letters from 

# 2020-05-26

4 hours

* Sorted results from language identification
* Swedish was rank 1 while English was rank 19 and French was rank 39
* Worked on generating random text program

# 2020-05-27

8 hours

* Worked on generating random text program
* Testing a few different methods of randomly select charaters
* Couldn't get a good way of creating random text using kenlm
* Working with probability values from arpa file to generate random text

# 2020-05-28

7 hours

* WSD Meeting
* Worked on generating random text program
* Cleaned up creating random text with unigram code
* Worked on creating random text with Bigrams and Trigrams
* Ran english decipherment program on dorabella

# 2020-05-29

7 hours

* Finished writing code to create random text with bigrams and trigrams
* Created random texts of various lengths and experimented with running language identification script on them

***

# 2020-06-01

7 hours

* Dorabella meeting and standup meeting
* Created a large text with random text generator program and passed the text back into kenlm
* Compared the new language model with the original language model used in the language generator
* Looking into youtube video about dorabella cipher theory

# 2020-06-02

5 hours

* Did some comparisons between arpa file obtained from Monday
* Noticed that the values from the random text's arpa file was very different from the original dorabella arpa file
* Wrote a python script to compare frequency of letters appearing in the random text to the unigram probability of the dorabella arpa
* Spent some time trying to figure out why kenlm was giving these odd numbers
* Did some tests calculating perplexity of texts with different models. 
* As expected, the perplexity of random text generated with the model was lower than an english text or a random text generated without the model

# 2020-06-03

7 hours

* Create 2000 random text files with 400000 characters in each. 1000 are generate from the arpa file and the other 1000 and generated randomly
* Started writing a program to score texts with kenlm and generate box plots
* Began writing another program to test claim from YouTube video that it is statistically impossible to see long sequences of characters with no repeats

# 2020-06-04

6 hours

* WSD Meeting and Standup
* Worked on program to test encoded texts for long sequences of non repeating bumps
* Texts around the same length as dorabella averaged around 10 characters in a row with no repeats
* Texts with 30 charaters averaged around 7.5 charaters in a row with no repeats
* Working on running more tests and graphing results

# 2020-06-05

6 hours

* Ran a test for longest sequence of no repeating bump
* Graphed the results of 100000 runs
* Started working on code to determine youtube claim about frequency of mirrored symbols

***

# 2020-06-08

6 hours

* Dorabella meeting and standup meetings
* Edited sequence and reflections code to work with Music notation
* Running reflection and longest sequence test on Music
* Running language identification script on dorabella again

# 2020-06-09

4 hours

* Creating and adding all my current work to a bit butcket repo
* Cleaning up code
* Ran tests on music

# 2020-06-10

8 hours

* Worked on changing code for running tests and graphing
* Ran longest sequence and mirror count code on english, french, and music
* Added results to google drive and created google sheets with values from the graphs

# 2020-06-11

6 hours

* WSD Meeting
* Reading about arpa files
* Rewriting some code to make it cleaner and run faster
* Testing results from random text generator

# 2020-06-12

3 hours

* AI Semenar
* Modifying dorabella ciphers fixed typo in orignal
* Uploaded dorabella and alternate dorabella to google drive
* Performed language identification on new text

# 2020-06-13

3 hours

* Finished writing random text generator that uses kenlm scored
* Summarized results from random text generator test to google drive

***

# 2020-06-15

7 hours

* Dorabella meeting and standup meetings
* NLP Seminar
* Read paper Automobiles - Driverless cars show the limits of today’s AI
* Created multiple alternate versions of dorabella to test with
* Uploaded to shared drive and gave explaination on each alternate version in a readme file
* Ran language identification on all 5 alternate versions
* Began writing a program to compare these results

# 2020-06-16

4 hours

* Ran decipherment script on all 6 different decipherments of dorabella
* Fixed language identification results
* Tried to add music corpus to language identification

# 2020-06-17

7 hours

* Meeting and standup
* Removed old incorrect transcriptions of dorabella from google drive
* Used consensus transcription of dorabella to create new transcriptions for dorabella and alt_dorabella
* Added new transcriptions and the key used for my transcription of dorabella to google drive
* Wrote python program to easily switch between different transcriptions of dorabella
* Looked into using Unravel to create new language models

# 2020-06-18

6 hours

* WSD Meeting
* Ran decipherment and language identification on corrected dorabella transcriptions
* Spent a few hours trying to install unravel
* Read unravel paper
* Reread Solving Substitution Ciphers with Combined Language Models

# 2020-06-19

6 hours

* Ran beamsearch decipherment scripts on corrected dorabella transcriptions
* Added gutenberg dangerous connections english polish and french texts to training langid data
* Ran unigram language identification method on dorabella
* Reran language model identification method on dorabella
* Reran both methods of language identification on original dorabella transcription now with the new gutenberg texts included in the training data
* Added results to google drive and began writing a summary of the results 
* Compared old dorabella transcript results with corrected dorabella results
* Spent some time trying to fix errors when installing unravel

***

# 2020-06-22

5 hours

* Music decipherment and standup meetings
* Went through files in anirudh repo found program to convert midi to text
* Converted an elgar song to text 
* Trained a language model with this text

# 2020-06-23

4 hours

* Wrote a program to test the average number of distinct characters in a 87 length text
* Results showed that only about 0.007% of texts had more than 24 distinct characters. Texts averaged around 19.9 distinct characters
* Looked into how to use the three different masc solvers. Encoded a piece of english text and attempted to decode with all three

# 2020-06-24

7 hours

* Wrote a program to take samples of n length samples from a texts
* Wrote a program to encipher these text samples with masc
* Wrote a program to calculate decipherment accuracy
* Took 5000 samples of 87 length and encoded these texts with a masc
* Wrote a bash script to run the decipherment programs on them. Will leave running over night 
* Ran a quick decipherment accuracy test with the few decipherments I have so far

# 2020-06-25

7 hours

* WSD and Standup meetings
* Testing nuhn solver trying to get it to decipher ciphers without spaces
* Wrote script to run decipherment tests on nuhn solver
* Wrote a program to summarize the results of decipherment accuracy test for the three solvers
* Enciphered some music files with masc program
* Figuring out how to train a music lm to use for the solvers
* Continued running decipherment tests to generate more samples

# 2020-06-26

6 hours

* Got nuhn solver to work on ciphers without spaces
* Wrote script to generate decipherments using nuhn solver and left running 
* Experimenting with music word language models 
* Testing decipherment of enciphered music with hhk solver
* Wrote program to split up music into bars

# 2020-06-27

1 hours

* Ran decipherments accuracy program for the decipherments obtained from the three solvers

***

# 2020-06-29

8 hours

* Dorabella and Standup Meetings
* Looked at norvig solver code
* Tested norvig solver without word lm 
* Read norvig chapter 14
* Read dorabella paper

# 2020-06-30

8 hours

* Finished reading norvig chapter 14
* Wrote program to decipher rail fence of different lengths
* Used rail fence program to created 40 transcriptions of dorabella and dorabella_alt (2-41 rails)
* Ran dorabella through norvig solver to decipher the unscrambled railfence transcriptions
* Create kenlm language model with english letters corpus
* Wrote program to run 80 decipherments through kenlm and sort decipherments by most likely 

# 2020-07-02

7 hours

* WSD, Decipherment and standup meetings 
* Writing program to generated language models for norvig solver 
* Used program to generate english language model and tested deciphering results
* Testing and fixing bugs with language model
* Created and testing a language model for norvig solver using New York Times 

# 2020-07-03

2 hours

* Working on program to generate language model for norvig solver
* Got a working language model for the norvig solver
* Cleaned extras out of Letters of Jane Austin
* Took 87 length samples from Letters of Jane Austin
* Enciphered Letters of Jane Austin samples with substitution cipher

***

# 2020-07-06

7 hours

* Dorabella and Standup Meetings
* Fixed bugs in norvig language model program
* Reran norvig decipherment tests on frankenstein and dracula using new york times language model
* Updated google drive spreadsheet comparing decipherment accuracy of NYT language model vs Google Trillion Words language model
* Changed text sampler to take samples from beginning of sentences
* Retook samples from Letters of Jane Austin and Dracula/Frankenstein
* Read Bayesian Inference for Zodiac and Other Homophonic Ciphers

# 2020-07-07

6 hours

* Learning how to use docker
* Got unravel working on my machine
* Figuring out how to use unravel and tested deciphering a text with Unravel
* Training language models for the three solvers using gutenberg english letters corpus 
* Setting up some decipherments tests to run on coronation

# 2020-07-08

6 hours

* Decipherment Meeting
* Finished generating and testing language models for two solvers. Stuck on getting pattern list for hhk solver
* Running decipherment test on Letters of Jane Austin Samples
* Aggregated result for solvers that finished running
* Learning how to use unravel solver and using docker

# 2020-07-09

7 hours

* WSD Meeting
* Figuring out error with creating language model for hhk solver
* Finished running tests for decipherment accuracy of norvig and nuhn solver
* Aggregated and updated spreadsheet with results
* Create google doc with useful links in shared drive. Helps make finding stuff easier
* Setting up decipherment tests for unravel solver

# 2020-07-10

4 hours

* Started running hhk solver decipherments on coronation
* Spent the rest of my day trying to figure out how to use unravel on Letter ciphers

***

# 2020-07-13

6 hours

* Dorabella and Standup Meetings
* Modified hhk decipherment scripts on coronation to automatically run 8 decipherments at a time in parallel
* Figuring out unravel

# 2020-07-14

5 hours

* Wrote program to convert nospace ciphers to the format required for unravel
* Wrote program to use unravel output to decipher plain text
* Trained Letters Corpus 8 gram language model for unravel

# 2020-07-15

8 hours

* Testing out different order language models. 8 gram model trained on NYT did not work very well 
* Trained 4 gram NYT language model for unravel
* Planning and writing scripts to process input/output and run decipherment tests with unravel easily

# 2020-07-16

7 hours

* WSD Meeting
* Finished creating unravel bash scripts for running tests
* Created and tested unravel with 3,4, and 5 gram language models
* Ran decipherements tests with NYT model and english letters model
* Calculated key and decipherment accuracy. Added results to google drive spreadsheet
* Unravel decipherment accuracy is not very impressive on ciphers without spaces


# 2020-07-17

4 hours

* Splitting NYT training corpus and taking samples to create a test set
* Running experiments to test in domain accuracy of det_unravel
* Figuring out how to use em_unravel

***

# 2020-07-20

6 hours

* Dorabella and Standup Meetings
* Took current HHK results from tests running on coronation and aggregated the decipherments to get a temporary accurracy
* Read through paper and started summarizing experiments I've done

# 2020-07-21

6 hours

* Fixing decipherment tests on coronation
* Starting second decipherment test on coronation
* Adding information about experiments into dorabella paper

# 2020-07-22

6 hours

* Testing HHK solver decipherment accuracy on letter samples using NYT LM
* Adding charts to dorabella paper 
* Trained a music LM for norvig solver
* Took a 200 note music sample and attempted to decipher it with norvig

# 2020-07-23

6 hours

* WSD Meeting
* Testing HHK solver with smaller NYT Language Model 
* Created language model with a truncated NYT corpus to match English Letters corpus length
* Setting up decipherment test with this new NYT model on coronation
* Adding information about this experiment to dorabella paper
* Took a piece of bach music and ran midi to text on it
* Took multiple 87 length samples from this corpus
* Enciphered these samples with MASC

# 2020-07-24

6 hours

* Modifying norvig LM code to generate language model with music file
* Experimenting with decoding music. Norvig solver requires a word language model but it's not very clear how to split music into words.
* Testing separating music corpus into words for training word language model
* Modified some code in norvig solver to only use character language model
* Testing norvig solver without word language model on english sample
* Generating character language model with bach music
* Testing decipherment with this model on music samples

***

# 2020-07-27

7 hours

* Dorabella and Standup Meetings
* Wrote program to map music to characters
* Used program to turn music training corpus and samples to characters
* Trained character language model with bach
* Tested solving music with modified norvig solver 

# 2020-07-28

5 hours

* Took hhk results from coronation and added the results to spreadsheet
* Tested modified norvig solver decipherment accuracy with only character language model
* Working on getting norvig solver to work with music

# 2020-07-29

7 hours

* Fixed issue with norvig solver when using both lower and uppercase characters
* Tested deciphering a 1000 length sample of bach music. Was only able to achieve an 83 percent decipherment accuracy
* Found more bach music and took multiple 87 length and 200 length samples
* Made modifications to decipherment accuracy program to make it easier to run
* Ran norvig solver on these samples and added decipherment accuracy to spreadsheet

# 2020-07-30

6 hours

* WSD Meeting
* Fixed an error in the way I was creating the test samples
* Ran decipherment tests on 200 length samples. Accuracy went up but is still very low
* Found multiple pieces of elgar music. Converted midi file to text, and mapped the text to characters
* Taking 87 length samples from elgar music
* Training norvig language model on elgar music

# 2020-07-30

5 hours

* Writing a script to help with preparing test samples from midi files
* Ran decipherment test on elgar music
* Got results for test using HHK solver with NYT lm from coronation

***

# 2020-08-04

7 hours

* Standup Meetings
* Found multiple beethoven and elgar midi files and converted them to text
* Concatenated them together to create a large corpus for training a Elgar LM and a Beethoven LM
* Used remaining midi files that weren't used to train the LM and took multiple 87 length samples 
* Ran decipherment tests on these samples

# 2020-08-05

6 hours

* Found large corpus of bach midi files
* Created script to process corpus of midi files and easily create a norvig LM
* Worked on script to create samples from corpus of midi files
* Ran LM script to generate Bach LM, language model was still pretty small

# 2020-08-06

6 hours

* WSD Meeting
* Running decipherment test using new bach LM
* Finding more bach music to train the LM
* Creating entropy test
* Created kenlm model with bach music

# 2020-08-07

5 hours

* Finished entropy test
* Found more bach and elgar music
* Ran decipherment tests with large elgar LM
* Ran decipherment test with a Bach + Elgar LM

***

# 2020-08-10

6 hours

* Dorabella and Standup Meetings
* Reduced number of unique characters when encoding music by 12 and ran decipherment tests on that
* Learning about how to transpose music to different keys
* Working on code to transpose midi files to the key of C
* Counting how many unique notes when song is transposed

# 2020-08-11

5 hours

* Combined flats and sharps that represent the same note into a single note
* Did a frequency analysis of the notes that appear when various pieces of Bach music are transposed to the key of C
* Took the 8 most common notes (A, B, C, D, E, F, G, and F#) multiplied by 3 durations to get a 24 characters for encoding the music 
* Notes that aren't in the 8 most common are moved a half step up or down (B# -> B)
* Tested enciphering and deciphering Bach music using this set of 24 symbols

# 2020-08-12

8 hours

* Decipherment Meeting
* Made modifications to norvig solver to easily swap between different character sets
* Looked through Bach24 decipherment test to check for any errors
* Found out I forgot to swap to the correct LM for the test
* Made modifications to norvig solver to take a LM as input when running
* Reran Bach24 decipherment test and added results to google drive spreadsheet
* Took dorabella cipher wrote code to scramble into 1000 different permutations
* Ran norvig solver to decipher the 1000 permutations

# 2020-08-13

6 hours

* WSD meeting
* Created kenlm model with Bach24
* Wrote program to calculate perplexity of 1000 random dorabella permutations
* Uploaded results to google drive. 
Original dorabella ranked 673 out of the 1001 permutations (1001 because 1000 scramble + original dorabella cipher)
and ranked 334 out of the 1001 deciphered permutations
* Created Elgar samples using 24 character set
* Working on perplexity test with Bach24 LM


# 2020-08-14

5 hours

* Ran perplexity tests and uploaded results to google drive
* Did frequency analysis of english and music LM training set
* Graphed results of frequency analysis
* Translated characters back to music symbols
* Looked for more midi files online

***

# 2020-08-17

6 hours

* Dorabella and Standup Meetings
* Modified norvig LM code to run faster
* Created LM with larger bach and elgar corpus and reran decipherment tests
* Ran frequency analysis on all music symbols
* Figuring out how to use Anirudh's solver

# 2020-08-18

8 hours

* Modified anirudh's solver to take a LM and Cipher as arguments
* Removed unnecessary output from anirudh's solver
* Generated LM for solver using English Letters as the training corpus
* Formatted text samples into input that the solver expects
* Ran English decipherment tests
* Formatted the solver's output and calculated decipherment accuracy
* Started working on Music decipherment with Anirudh's solver

# 2020-08-19

8 hours

* Decipherment Meeting
* Preparing music samples for anirudh's solver 
* Training Bach LM and a Elgar LM using 24 symbol training corpus
* Running decipherment Tests for 24 symbol music
* Looking into anirudh music solvers
* Modified anirudh's english solver to work with 51 symbols
* Created music samples with all 51 notes 
* Ran decipherment tests on 51 symbol music
* Calculated decipherment accuracy added results to google spread sheet
* Found an error running the 51 symbol decipherment test. Fixed and rerunning the test

# 2020-08-20

7 hours

* WSD Meeting
* Fixed error with music decipherment output and updated the results on google spreadsheet
* Created English, Music, and Dorabella samples for 1000 random shuffles experiment
* Added permutations for english and music decipherment of dorabella
* Trained music LM with bach corpus and trained a English LM with Letters Corpus
* Wrote and ran tests
* Added results from tests to google spreadsheet

# 2020-08-21

1 hours

* Added music to language identification program
* Ran langID test 
* Frequency analysis of dorabella cipher
* Wrote a README for 1000 random shuffles experiment

***

# 2020-08-24

6 hours

* Dorabella and Standup Meetings
* Sorted language identification results and added to spreadsheet 
* Tested deciphering dorabella cipher one line at a time
* Tested deciphering dorabella cipher two lines at a time 
* Looked for overlapping mappings
* Started generating samples for language ID experiement
* Oragnizing code and writing descriptions for each

# 2020-08-25

9 hours

* Preparing 6500 length training text for language identification program
* Enciphered 6500 length english text for training
* Preparing multiple 87 length samples of music and english
* Wrote script to run language identification tests
* Wrote program to aggregate results from the tests
* Running language identification on the multiple samples overnight

# 2020-08-26

7 hours

* Decipherment Meeting
* Yourui's thesis presentation
* Working on program to format output of run language identification tests
* Filtering out results with alphabet size mismatch. Getting 100 Samples for each test Language
* Created 2 alternate encodings of dorabella and ran language identification test on those
* Working on script to aggregate results

# 2020-08-27

8 hours

* Finishing script to aggregate results
* Finished getting all results from unigram and lmopt language id experiment and adding to google spread sheet
* WSD Meeting
* Setting up final language identification test
* Organizing and writing READMEs for code

# 2020-08-28

2 hours

* Organizing and documenting code. 